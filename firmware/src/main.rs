#![no_std]
#![no_main]

use core::{arch::global_asm, panic::PanicInfo, fmt::Write, convert::Infallible};

use ufmt::{uwrite, uWrite};

global_asm!(include_str!("init.s"));


/// A panic handler is required in Rust, this is probably the most basic one possible
#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {}
}

const LED_BASE: *mut u8 = 4096 as *mut u8;

fn set_led(on: bool) {
    let val = if on {1} else {0};

    unsafe {
        LED_BASE.write_volatile(val)
    }
}

const TIMER_BASE: *mut u32 = 5120 as *mut u32;
// Offsets in sizeof(u32)
const OFFSET_WRAP_TIME: isize = 0;
const OFFSET_TIMER: isize = 1;
const OFFSET_FLAG: isize = 2;

fn init_timer(cycles: u32) {
    unsafe {
        TIMER_BASE.offset(OFFSET_WRAP_TIME).write_volatile(cycles)
    }
}

fn wraparound() -> bool {
    unsafe {
        TIMER_BASE.offset(OFFSET_FLAG).read_volatile() == 1
    }
}

const TEXT_START: *mut u8 = 0x7000_0000 as *mut u8;

fn clear() {
    for i in 0..200*64 {
        unsafe {
            TEXT_START.offset(i).write_volatile(b' ')
        }
    }
}

fn write_bytes(chars: &[u8], y: isize) {
    for (i, c) in chars.iter().enumerate() {
        unsafe {
            TEXT_START.offset(i as isize + y * 200).write_volatile(*c as u8)
        }
    }
}

struct Text {
    y: isize
}
impl uWrite for Text {
    type Error = Infallible;
    fn write_str(&mut self, s: &str) -> Result<(), Self::Error> {
        write_bytes(s.as_bytes(), self.y);
        Ok(())
    }
}


/// Main program function
#[no_mangle]
extern "C" fn main() -> () {
    clear();
    write_bytes(b"Hello Rust and Spade!", 0);

    // Example: Create a counter peripheral with base address 0x8000_0000
    let mut val = false;
    init_timer(40_000_000);
    let mut time = 0;
    loop {
        if wraparound() {
            set_led(val);
            val = !val;

            time += 1;

            uwrite!(Text{y: 1}, "{}", time).ok();
        }
    }
}
