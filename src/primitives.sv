module launder_clock(input i, output output__);
  assign output__ = i;
endmodule

module hdmi_pll_impl(input clk_i, output clk_shift, output clk_pixel, output output__);
  wire clk_locked;
  wire [3:0] clocks;
  assign clk_shift = clocks[0];
  assign clk_pixel = clocks[1];
  ecp5pll
  #(
      .in_hz(25000000),
    .out0_hz(40000000*5),
    .out1_hz(40000000)
  )
  ecp5pll_inst
  (
    .clk_i(clk_i),
    .clk_o(clocks),
    .locked(clk_locked)
  );
endmodule



module oddrx1f_wrapper(
    input D0,
    input D1,
    input SCLK,
    input RST,
    output Q,
    output output__
);
  ODDRX1F inner  (.D0(D0), .D1(D1), .Q(Q), .SCLK(SCLK), .RST(RST));
endmodule
