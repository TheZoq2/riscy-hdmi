#top = tile_map::tile_map

from spade import *

from cocotb.clock import Clock

@cocotb.test()
async def tile_map_frame(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    s.i.offset = "0"
    s.i.bus = """&spadev::bus::ControlSignals$(
        addr: 0,
        access_width: spadev::bus::AccessWidth::Full(),
        cmd: spadev::bus::Command::Nop()
    )"""
    await FallingEdge(clk)

    for y in range(0, 20):
        print(f"Simulating y={y}")
        for x in range(0, 150):
            await FallingEdge(clk)
            s.i.pixel = f"({x}, {y})"
